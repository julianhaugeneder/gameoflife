// https://docs.cypress.io/api/introduction/api.html

describe("#1 Check if online", () => {
  it("Check if page loads without instant error", () => {
    cy.viewport(1920, 1080)
    cy.visit("/");
    cy.contains("h1", "Game of Life.");
    cy.contains("span", "Beschreibung");
    cy.contains("span", "Regeln");
    cy.contains("span", "John Horton Conway");
  });
});

describe("#2 Check if menu works", () => {
  it("Check if menu navigation works as expected", () => {
    cy.viewport(1920, 1080)
    cy.visit("/");
    cy.contains("span", "Beschreibung");
    cy.get("#menu1").click()
    cy.contains("p", "Das Spiel des Lebens (engl. Conway’s Game of Life) ist ein vom");
    cy.get("#menu2").click()
    cy.contains("p", "Das Spielfeld ist in Zeilen und Spalten unterteilt und im Idealfall unendlich groß.");
    cy.get("#menu3").click()
    cy.contains("p", "John Horton Conway (* 26. Dezember 1937 in Liverpool, Vereinigtes Königreich;");

  });
});

});
